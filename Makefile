DYNAMO_ENDPOINT=http://localhost:4566
DATA_PATH=./
dynamo:
	- aws --endpoint-url=${DYNAMO_ENDPOINT} dynamodb create-table --table-name users  --attribute-definitions AttributeName=username,AttributeType=S --key-schema AttributeName=username,KeyType=HASH --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1
	- aws --endpoint-url=${DYNAMO_ENDPOINT} dynamodb batch-write-item --request-items file://${DATA_PATH}/data.json
