module testing_with_testcontainers

go 1.14

require (
	github.com/aws/aws-sdk-go v1.35.13
	github.com/docker/go-connections v0.4.0
	github.com/stretchr/testify v1.6.1
	github.com/testcontainers/testcontainers-go v0.9.0
)
