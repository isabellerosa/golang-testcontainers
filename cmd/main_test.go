package main

import (
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRepository_Save(t *testing.T) {
	ctx := context.Background()

	var endpoint string

	container, port := StartDynamo(ctx)
	defer container.Terminate(ctx)
	endpoint = "http://localhost:" + port

	CreateDynamoTables(endpoint)

	mySession, _ := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Endpoint:   aws.String(endpoint),
			Region:     aws.String("us-east-1"),
			MaxRetries: aws.Int(1),
		},
	})

	svc := dynamodb.New(mySession)

	r := Repository{db: svc, cfg: &Config{TableName: "users"}}

	tests := []struct {
		name    string
		user    User
		wantErr bool
		errMsg  string
	}{
		{
			name: "should create user",
			user: User{
				Name:  "johnny",
				Email: "johnnybrave@here",
			},
			wantErr: false,
		},
		{
			name: "should give conditional check",
			user: User{
				Name:  "spider",
				Email: "peterspider@here",
			},
			wantErr: true,
			errMsg:  "ConditionalCheckFailedException",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := r.Save(ctx, tt.user)

			if tt.wantErr {
				assert.Error(t, err, "expected an error")
				//assert.Contains(t, err.Error(), tt.errMsg)
			} else {
				assert.NoError(t, err)
				count, _ := r.Contains(ctx, tt.user)
				assert.Equal(t, true, count)
			}
		})
	}
}
