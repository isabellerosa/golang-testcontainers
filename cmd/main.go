package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type (
	DynamoDBAPI interface {
		PutItemWithContext(aws.Context, *dynamodb.PutItemInput, ...request.Option) (*dynamodb.PutItemOutput, error)
		GetItemWithContext(aws.Context, *dynamodb.GetItemInput, ...request.Option) (*dynamodb.GetItemOutput, error)
	}

	Config struct {
		TableName string
	}

	User struct {
		Name  string
		Email string
	}

	Repository struct {
		cfg *Config
		db  DynamoDBAPI
	}
)

func (r Repository) Save(ctx context.Context, user User) error {
	input := dynamodb.PutItemInput{
		ConditionExpression: aws.String("attribute_not_exists(username)"),
		Item: map[string]*dynamodb.AttributeValue{
			"username": {S: aws.String(user.Name)},
			"email":    {S: aws.String(user.Email)},
		},
		TableName: aws.String(r.cfg.TableName),
	}

	_, err := r.db.PutItemWithContext(ctx, &input)

	return err
}

func (r Repository) Contains(ctx context.Context, user User) (bool, error) {
	input := dynamodb.GetItemInput{
		Key:            map[string]*dynamodb.AttributeValue{"username": {S: aws.String(user.Name)}},
		TableName:      aws.String(r.cfg.TableName),
		ConsistentRead: aws.Bool(true),
	}

	out, err := r.db.GetItemWithContext(ctx, &input)
	if err != nil {
		return false, fmt.Errorf("error finding user %v at table %s: %w", user, r.cfg.TableName, err)
	}

	return len(out.Item) > 0, nil
}
