package main

import (
	"context"
	"github.com/docker/go-connections/nat"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"log"
	"os"
	"os/exec"
	"path"
)

func StartDynamo(ctx context.Context) (testcontainers.Container, string) {
	return startContainer(
		ctx,
		"users_db",
		map[string]string{"SERVICES": "dynamodb"},
		"localstack/localstack:0.12.1",
		"4566/tcp",
	)
}

func startContainer(ctx context.Context, name string, envs map[string]string, image, servicePort string) (testcontainers.Container, string) {
	req := testcontainers.ContainerRequest{
		Name:         name,
		Image:        image,
		Env:          envs,
		ExposedPorts: []string{servicePort},
		WaitingFor:   wait.ForListeningPort(nat.Port(servicePort)),
	}

	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})

	if err != nil {
		log.Fatalf("Couldn't start container %s: %s", image, err)
	}

	port, err := container.MappedPort(ctx, nat.Port(servicePort))
	if err != nil {
		log.Fatalf("Error getting exposed ports: %s", err)
	}

	return container, port.Port()
}

func CreateDynamoTables(endpoint string) {
	root := getProjectDir()
	log.Print("Creating Dynamo tables....")
	callMakefile("DYNAMO_ENDPOINT="+endpoint, "DATA_PATH="+root, "dynamo")
}

func callMakefile(args ...string) {
	root := getProjectDir()
	makefile := root + "/Makefile"

	mkArgs := append(args, "-f", makefile)

	cmd := exec.Command("make", mkArgs...)
	if out, err := cmd.Output(); err != nil {
		log.Printf("%s \n\n %s", cmd.Stdout, out)
		log.Fatalf("Error executing task: %s", err)
	}
}

func getProjectDir() string {
	repositoryDir, _ := os.Getwd()
	relativeProjectDir := ".."
	return path.Join(repositoryDir, relativeProjectDir)
}
